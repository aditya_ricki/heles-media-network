<?php

use Illuminate\Support\Facades\Route;

/* ============================ MARKETING ============================= */
use App\Http\Controllers\Marketing\DashboardController as DashboardMarketing;
use App\Http\Controllers\Marketing\CustomerController as MarketingCustomer;

/* ============================ MARKETING ============================= */
use App\Http\Controllers\StaffTeknis\DashboardController as DashboardStaffTeknis;

/* ============================ FINANCE ============================= */
use App\Http\Controllers\Finance\DashboardController as DashboardFinance;

/* ============================ CUSTOMER LOGIN ============================= */
use App\Http\Controllers\Customer\DashboardController as DashboardCustomer;

/* ============================ CUSTOMER ============================= */
use App\Http\Controllers\HomeController;

/* ============================ OAUTH ============================= */
use App\Http\Controllers\Auth\GithubController;
use App\Http\Controllers\Auth\FacebookController;
use App\Http\Controllers\Auth\GoogleController;

\URL::forceScheme('https');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);

// Verifikasi email
Route::middleware('auth')->get('/email/verify', function () {
    return view('auth.verify');
})->name('verification.notice');

// Reset password
Route::get('/forgot-password', function () {
    return view('auth.passwords.email');
})->middleware('guest')->name('password.request');

/* ============================ OAUTH GITHUB ============================= */
Route::get('oauth/github/redirect', [GithubController::class, 'redirect'])->name('oauth.github.redirect');
Route::get('oauth/github/callback', [GithubController::class, 'callback'])->name('oauth.github.callback');

/* ============================ OAUTH FACEBOOK ============================= */
Route::get('oauth/fb/redirect', [FacebookController::class, 'redirect'])->name('oauth.fb.redirect');
Route::get('oauth/fb/callback', [FacebookController::class, 'callback'])->name('oauth.fb.callback');
Route::get('oauth/fb/privacy', [FacebookController::class, 'privacy'])->name('oauth.fb.privacy');
Route::get('oauth/fb/delete-data', [FacebookController::class, 'deleteData'])->name('oauth.fb.delete-data');

/* ============================ OAUTH GOOGLE ============================= */
Route::get('oauth/google/redirect', [GoogleController::class, 'redirect'])->name('oauth.google.redirect');
Route::get('oauth/google/callback', [GoogleController::class, 'callback'])->name('oauth.google.callback');

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
	/* ============================ MARKETING ============================= */
	Route::group(['prefix' => 'marketing', 'middleware' => 'marketing'], function () {
		Route::get('/', [DashboardMarketing::class, 'index'])->name('dashboard.marketing');

		Route::group(['prefix' => 'customer'], function () {
			Route::get('/', [MarketingCustomer::class, 'index'])->name('marketing.customer');
			Route::get('/create', [MarketingCustomer::class, 'create'])->name('marketing.customer.create');
			Route::post('/store', [MarketingCustomer::class, 'store'])->name('marketing.customer.store');
			Route::get('/edit/{id}', [MarketingCustomer::class, 'edit'])->name('marketing.customer.edit');
			Route::put('/update/{id}', [MarketingCustomer::class, 'update'])->name('marketing.customer.update');
			Route::get('/show/{id}', [MarketingCustomer::class, 'show'])->name('marketing.customer.show');
		});
	});

	/* ============================ STAFF TEKNIS ============================= */
	Route::group(['middleware' => 'staff-teknis'], function () {
		Route::get('/staff-teknis', [DashboardStaffTeknis::class, 'index'])->name('dashboard.staff-teknis');
	});

	/* ============================ FINANCE ============================= */
	Route::group(['middleware' => 'finance'], function () {
		Route::get('/finance', [DashboardFinance::class, 'index'])->name('dashboard.finance');
	});

	/* ============================ CUSTOMER LOGIN ============================= */
	Route::group(['middleware' => 'customer'], function () {
		Route::get('/customer', [DashboardCustomer::class, 'index'])->name('dashboard.customer');
	});
});

/* ============================ CUSTOMER ============================= */
Route::get('/', [HomeController::class, 'index'])->name('landing-page');
