<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Finance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if ($request->user() && $request->user()->role->code == 'FNC') {
                return $next($request);
            }

            return redirect()->route('login');
        } catch (\Throwable $th) {
            writeLog($th);
        }
    }
}
