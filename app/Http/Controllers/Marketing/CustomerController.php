<?php

namespace App\Http\Controllers\Marketing;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $customers = User::with('role', 'customer')
                         ->where('role_id', env('CST'))
                         ->get();

            return view('dashboard-marketing.customers.index', compact('customers'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('dashboard-marketing.customers.create');
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'phone'     => 'required',
            'whatsapp'  => 'required',
            'photo'     => 'file|mimes:png,jpg',
            'photo_ktp' => 'required|file|mimes:png,jpg',
            'address'   => 'required',
            'longitude' => 'required',
            'latitude'  => 'required',
        ]);

        try {
            DB::beginTransaction();

            $name      = $request->name;
            $email     = $request->email;
            $phone     = $request->phone;
            $whatsapp  = $request->whatsapp;
            $photo     = $request->file('photo') ?: '';
            $photo_ktp = $request->file('photo_ktp') ?: '';
            $address   = $request->address;
            $longitude = $request->longitude;
            $latitude  = $request->latitude;

            $user = User::create([
                'name'     => $name,
                'email'    => $email,
                'password' => Hash::make(date('Ymd')),
                'role_id'  => 4,
            ]);

            $customer = Customer::create([
                'user_id'   => $user->id,
                'phone'     => $phone,
                'whatsapp'  => $whatsapp,
                'photo'     => !$photo ?: uploadFile($photo, 'photo'),
                'photo_ktp' => !$photo_ktp ?: uploadFile($photo_ktp, 'photo_ktp'),
                'address'   => $address,
                'longitude' => $longitude,
                'latitude'  => $latitude,
            ]);

            DB::commit();
            return redirect()->route('marketing.customer')->with('success', 'Berhasil tambah customer');
        } catch (\Throwable $th) {
            DB::rollback();
            writeLog($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $customer = Customer::with('user')
                        ->where('user_id', $id)
                        ->first();

            if (!$customer) {
                return redirect()->back()->with('error', 'Data customer tidak ditemukan');
            }

            return view('dashboard-marketing.customers.show', compact('customer'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $customer = Customer::with('user')
                        ->where('user_id', $id)
                        ->first();

            if (!$customer) {
                return redirect()->back()->with('error', 'Data customer tidak ditemukan');
            }

            return view('dashboard-marketing.customers.edit', compact('customer'));
        } catch (\Throwable $th) {
            writeLog($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required|email',
            'phone'     => 'required',
            'whatsapp'  => 'required',
            'photo'     => 'file|mimes:png,jpg',
            'photo_ktp' => 'file|mimes:png,jpg',
            'address'   => 'required',
            'longitude' => 'required',
            'latitude'  => 'required',
        ]);

        try {
            DB::beginTransaction();

            $name      = $request->name;
            $email     = $request->email;
            $phone     = $request->phone;
            $whatsapp  = $request->whatsapp;
            $address   = $request->address;
            $longitude = $request->longitude;
            $latitude  = $request->latitude;
            $customer  = Customer::with('user')
                         ->where('user_id', $id)
                         ->first();

            if (!$customer) {
                return redirect()->back()->with('error', 'Data customer tidak ditemukan');
            }

            $customer->user->update([
                'name'  => $name,
                'email' => $email,
            ]);

            if ($request->file('photo')) {
                $photo = uploadFile($request->file('photo'), 'photo');
                deleteFile($customer->photo);
            } else {
                $photo = $customer->photo;
            }

            if ($request->file('photo_ktp')) {
                $photo_ktp = uploadFile($request->file('photo_ktp'), 'photo_ktp');
                deleteFile($customer->photo_ktp);
            } else {
                $photo_ktp = $customer->photo_ktp;
            }

            $customer->update([
                'phone'     => $phone,
                'whatsapp'  => $whatsapp,
                'photo'     => $photo,
                'photo_ktp' => $photo_ktp,
                'address'   => $address,
                'longitude' => $longitude,
                'latitude'  => $latitude,
            ]);

            DB::commit();
            return redirect()->route('marketing.customer')->with('success', 'Berhasil ubah customer');
        } catch (\Throwable $th) {
            DB::rollback();
            writeLog($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subscriber  $subscriber
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscriber $subscriber)
    {
        //
    }
}
