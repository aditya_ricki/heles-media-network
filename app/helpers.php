<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

function writeLog($message)
{
    Log::error($message);
    abort(500);
}

function uploadFile($file, $path, $isBase64 = false)
{
	try {
		if ($isBase64) {
			$path = str_replace('_', '-', $path) . '/';
		    $file = base64_decode(str_replace('base64,', '', explode(';', $file)[1]), true);
		    $name = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 15) . '.webp';

		    Storage::disk(env('STORAGE', 'local'))->put('public/' . $path . $name, $file);

	    	return 'storage/' . $path . $name;
		} else {
			$path    = str_replace('_', '-', $path);
			$ext     = $file->getClientOriginalExtension();
			$newFile = Storage::disk(env('STORAGE', 'local'))->put('public/' . $path, $file);
			$arr     = explode('/', $newFile);

	    	return 'storage/' . $path . '/' . end($arr);
		}
	} catch (\Throwable $th) {
	    Log::error($th->getMessage());
	    abort(500);
	}
}

function deleteFile($path)
{
	try {
    	return unlink(public_path($path));
	} catch (\Throwable $th) {
	    Log::error($th->getMessage());
	    abort(500);
	}
}
