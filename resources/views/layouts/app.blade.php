<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>
            @yield('title')
        </title>

        @include('layouts.css')

        @yield('custom-css')
    </head>

    <body>
        @include('layouts.navbar')

        <div class="main-content">
            @yield('main-content')
        </div>

        @include('layouts.footer')

        @include('layouts.js')

        @yield('custom-js')
    </body>
</html>