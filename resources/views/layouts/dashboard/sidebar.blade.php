<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="../../pages/dashboards/dashboard.html">
                <img src="{{ asset('img/brand/blue.png') }}" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    @if(Auth::user()->role->code == 'MRT')
                        @include('layouts.dashboard.sidebar.marketing')
                    @elseif(Auth::user()->role->code == 'STT')
                        @include('layouts.dashboard.sidebar.staff-teknis')
                    @elseif(Auth::user()->role->code == 'FNC')
                        @include('layouts.dashboard.sidebar.finance')
                    @else
                        @include('layouts.dashboard.sidebar.customer')
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>