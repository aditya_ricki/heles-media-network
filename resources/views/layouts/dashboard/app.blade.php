<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>
            @yield('title')
        </title>

        @include('layouts.dashboard.css')

        @yield('custom-css')
    </head>

    <body>
        @include('layouts.dashboard.sidebar')

        <div class="main-content" id="panel">
            @include('layouts.dashboard.navbar')

            @if(session('success'))
                <script>
                    let text = '{{ session('success') }}'

                    Swal.fire({
                        title: 'Berhasil',
                        text: text,
                        icon: 'success'
                    })
                </script>
            @elseif(session('error'))
                <script>
                    let text = '{{ session('error') }}'

                    Swal.fire({
                        title: 'Peringatan',
                        text: text,
                        icon: 'error'
                    })
                </script>
            @endif

            @yield('main-content')
        </div>

        @include('layouts.dashboard.js')

        @yield('custom-js')
    </body>
</html>