@extends('layouts.auth.app')

@section('title', 'Register')

@section('main-content')
<div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
  <div class="container">
    <div class="header-body text-center mb-7">
      <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
          <h1 class="text-white">Create an account</h1>
          <p class="text-lead text-white">Use these awesome forms to login or create new account in your project for free.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>
<div class="container mt--8 pb-5">
  <!-- Table -->
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-12 col-sm-12">
      <div class="card bg-secondary border-0">
        <div class="card-header bg-transparent pb-5">
          <div class="text-muted text-center mt-2 mb-4"><small>Sign up with</small></div>
          <div class="text-center">
            <a href="{{ route('oauth.github.redirect') }}" class="btn btn-neutral btn-icon mb-2">
              <span class="btn-inner--icon"><img src="{{ asset('img/icons/common/github.svg') }}"></span>
              <span class="btn-inner--text">Github</span>
            </a>
            <a href="{{ route('oauth.fb.redirect') }}" class="btn btn-neutral btn-icon mb-2">
              <span class="btn-inner--icon"><img src="{{ asset('img/icons/common/fb.png') }}"></span>
              <span class="btn-inner--text">Facebook</span>
            </a>
            <a href="{{ route('oauth.google.redirect') }}" class="btn btn-neutral btn-icon mb-2">
              <span class="btn-inner--icon"><img src="{{ asset('img/icons/common/google.svg') }}"></span>
              <span class="btn-inner--text">Google</span>
            </a>
          </div>
        </div>
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Or sign up with credentials</small>
          </div>
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <form role="form" method="post" action="{{ route('register') }}">
            @csrf
            @method('POST')
            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                </div>
                <input class="form-control" placeholder="Name" type="text" name="name" value="{{ old('name') }}">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                </div>
                <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input class="form-control" placeholder="Password" type="password" name="password">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group input-group-merge input-group-alternative">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                </div>
                <input class="form-control" placeholder="Password Confirmation" type="password" name="password_confirmation">
              </div>
            </div>
            <div class="row my-4">
              <div class="col-12">
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input check-privacy-policy" id="customCheckRegister" type="checkbox">
                  <label class="custom-control-label check-privacy-policy" for="customCheckRegister">
                    <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                  </label>
                </div>
              </div>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary mt-4" id="submit" disabled>Create account</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('custom-js')
<script>
  $(document).ready(function () {
    $('.check-privacy-policy').on('click', function () {
      if ($('#customCheckRegister').is(':checked')) {
        $('#submit').prop('disabled', false)
      } else {
        $('#submit').prop('disabled', true)
      }
    })
  })
</script>
@endsection
