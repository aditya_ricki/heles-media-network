@extends('layouts.app')

@section('title', 'Heles Media Network')

@section('main-content')
<div class="header bg-primary pt-5 pb-7">
    <div class="container">
        <div class="header-body">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="pr-5">
                        <h1 class="display-2 text-white font-weight-bold mb-0">Argon Dashboard PRO</h1>
                        <h2 class="display-4 text-white font-weight-light">A beautiful premium dashboard for Bootstrap 4.</h2>
                        <p class="text-white mt-4">Argon perfectly combines reusable HTML and modular CSS with a modern styling and beautiful markup throughout each HTML template in the pack.</p>
                        <div class="mt-5">
                            <a href="./pages/dashboards/dashboard.html" class="btn btn-neutral my-2">Explore Dashboard</a>
                            <a href="https://www.creative-tim.com/product/argon-dashboard-pro" class="btn btn-default my-2">Purchase now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row pt-5">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Components</h5>
                                    <p>Argon comes with over 70 handcrafted components.</p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Plugins</h5>
                                    <p>Fully integrated and extendable third-party plugins that you will love.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pt-lg-5 pt-4">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Pages</h5>
                                    <p>From simple to complex, you get a beautiful set of 15+ page examples.</p>
                                </div>
                            </div>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow mb-4">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                    <h5 class="h3">Documentation</h5>
                                    <p>You will love how easy is to to work with Argon.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section section-lg pt-lg-0 mt--7">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card card-lift--hover shadow border-0">
                            <div class="card-body py-5">
                                <div class="icon icon-shape bg-gradient-primary text-white rounded-circle mb-4">
                                    <i class="ni ni-check-bold"></i>
                                </div>
                                <h4 class="h3 text-primary text-uppercase">Based on Bootstrap 4</h4>
                                <p class="description mt-3">Argon is built on top of the most popular open source toolkit for developing with HTML, CSS, and JS.</p>
                                <div>
                                    <span class="badge badge-pill badge-primary">bootstrap 4</span>
                                    <span class="badge badge-pill badge-primary">dashboard</span>
                                    <span class="badge badge-pill badge-primary">template</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-lift--hover shadow border-0">
                            <div class="card-body py-5">
                                <div class="icon icon-shape bg-gradient-success text-white rounded-circle mb-4">
                                    <i class="ni ni-istanbul"></i>
                                </div>
                            <h4 class="h3 text-success text-uppercase">Integrated build tools</h4>
                                <p class="description mt-3">Use Argons's included npm and gulp scripts to compile source code, run tests, and more with just a few simple commands.</p>
                                <div>
                                    <span class="badge badge-pill badge-success">npm</span>
                                    <span class="badge badge-pill badge-success">gulp</span>
                                    <span class="badge badge-pill badge-success">build tools</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-lift--hover shadow border-0">
                            <div class="card-body py-5">
                                <div class="icon icon-shape bg-gradient-warning text-white rounded-circle mb-4">
                                    <i class="ni ni-planet"></i>
                                </div>
                                <h4 class="h3 text-warning text-uppercase">Full Sass support</h4>
                                <p class="description mt-3">Argon makes customization easier than ever before. You get all the tools to make your website building process a breeze.</p>
                                <div>
                                    <span class="badge badge-pill badge-warning">sass</span>
                                    <span class="badge badge-pill badge-warning">design</span>
                                    <span class="badge badge-pill badge-warning">customize</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6 order-md-2">
                <img src="{{ asset('img/theme/landing-1.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5">
                    <h1>Awesome features</h1>
                    <p>This dashboard comes with super cool features that are meant to help in the process. Handcrafted components, page examples and functional widgets are just a few things you will see and love at first sight.</p>
                    <ul class="list-unstyled mt-5">
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-settings-gear-65"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Carefully crafted components</h4>
                                </div>
                            </div>
                        </li>
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-html5"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Amazing page examples</h4>
                                </div>
                            </div>
                        </li>
                        <li class="py-2">
                            <div class="d-flex align-items-center">
                                <div>
                                    <div class="badge badge-circle badge-success mr-3">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div>
                                    <h4 class="mb-0">Super friendly support team</h4>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6">
                <img src="{{ asset('img/theme/landing-2.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6">
                <div class="pr-md-5">
                    <h1>Example pages</h1>
                    <p>If you want to get inspiration or just show something directly to your clients, you can jump start your development with our pre-built example pages.</p>
                    <a href="./pages/examples/profile.html" class="font-weight-bold text-warning mt-5">Explore pages</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="py-6">
    <div class="container">
        <div class="row row-grid align-items-center">
            <div class="col-md-6 order-md-2">
                <img src="{{ asset('img/theme/landing-3.png') }}" class="img-fluid">
            </div>
            <div class="col-md-6 order-md-1">
                <div class="pr-md-5">
                    <h1>Lovable widgets and cards</h1>
                    <p>We love cards and everybody on the web seems to. We have gone above and beyond with options for you to organise your information. From cards designed for content, to pricing cards or user profiles, you will have many options to choose from.</p>
                    <a href="./pages/widgets.html" class="font-weight-bold text-info mt-5">Explore widgets</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
