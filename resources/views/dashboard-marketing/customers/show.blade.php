@extends('layouts.dashboard.app')

@section('title', 'Customers | HMN')

@section('custom-css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@endsection

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Customers</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('landing-page') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('marketing.customer') }}">Customers</a></li>
              <li class="breadcrumb-item active" aria-current="page">Show</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('marketing.customer') }}" class="btn btn-sm btn-neutral"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-6 col-md-6 col-sm-6">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-hover table-striped table-borderless">
              <tr>
                <th>Nama</th>
                <td>: {{ $customer->user->name }}</td>
              </tr>
              <tr>
                <th>E-Mail</th>
                <td>: {{ $customer->user->email }}</td>
              </tr>
              <tr>
                <th>No. Telepon</th>
                <td>: {{ $customer->phone }}</td>
              </tr>
              <tr>
                <th>Whatsapp</th>
                <td>: {{ $customer->whatsapp }}</td>
              </tr>
              <tr>
                <th>Foto</th>
                <td>
                  <a data-fancybox="gallery" href="{{ asset($customer->photo) }}">
                    <img src="{{ asset($customer->photo) }}" alt="{{ $customer->photo }}" class="img img-thumbnail">
                  </a>
                </td>
              </tr>
              <tr>
                <th>Foto KTP</th>
                <td>
                  <a data-fancybox="gallery" href="{{ asset($customer->photo_ktp) }}">
                    <img src="{{ asset($customer->photo_ktp) }}" alt="{{ $customer->photo_ktp }}" class="img img-thumbnail">
                  </a>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-6 col-sm-6">
      <div class="card">
        <div class="card-body">
            <table class="table table-hover table-striped table-borderless">
              <tr>
                <th>Alamat</th>
                <td>: {{ $customer->address }}</td>
              </tr>
              <tr>
                <th>Longitude</th>
                <td>: {{ $customer->longitude }}</td>
              </tr>
              <tr>
                <th>Latitude</th>
                <td>: {{ $customer->latitude }}</td>
              </tr>
              <tr>
                <td colspan="2">
                  <div style="width: 100%; height: 480px" id="map-container"></div>
                </td>
              </tr>
            </table>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
<script>
  $(document).ready(function () {
    navigator.geolocation.getCurrentPosition(function(position) {
      let lat  = '{{ $customer->latitude }}'
      let long = '{{ $customer->longitude }}'

      function addDraggableMarker(peta, behavior){
        // create marker
        let marker = new H.map.Marker({
          lat: lat,
          lng: long
        },
        {
          // mark the object as volatile for the smooth dragging
          volatility: true
        })

        // Add the marker to the map and center the map at the location of the marker:
        peta.addObject(marker)

        // Ensure that the marker can receive drag events
        marker.draggable = true
        peta.addObject(marker)

        // disable the default draggability of the underlying map
        // and calculate the offset between mouse and target's position
        // when starting to drag a marker object:
        peta.addEventListener('dragstart', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer

          if (target instanceof H.map.Marker) {
            let targetPosition = peta.geoToScreen(target.getGeometry())
            target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y)
            behavior.disable()
          }
        }, false)

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        peta.addEventListener('dragend', function(ev) {
          let target = ev.target
          document.getElementById('longitude').value = target.b.lng
          document.getElementById('latitude').value = target.b.lat

          if (target instanceof H.map.Marker) {
            behavior.enable()
          }
        }, false)

        // Listen to the drag event and move the position of the marker
        // as necessary
        peta.addEventListener('drag', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer
          if (target instanceof H.map.Marker) {
            target.setGeometry(peta.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y))
          }
        }, false)
      }

      let platform = new H.service.Platform({
        'apikey': '{{ env('HEREMAPS_API_KEY') }}'
      })

      // Obtain the default map types from the platform object:
      let defaultLayers = platform.createDefaultLayers()

      // Instantiate (and display) a map object:
      let peta = new H.Map(
        document.getElementById('map-container'),
        defaultLayers.vector.normal.map,
        {
          zoom: 15,
          center: {
            lat: lat,
            lng: long
          }
        }
      )

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => peta.getViewPort().resize())

      // Add map events functionality to the map
      let mapEvents = new H.mapevents.MapEvents(peta)

      // Add behavior to the map: panning, zooming, dragging.
      let behavior = new H.mapevents.Behavior(mapEvents)

      // map ui
      let ui = H.ui.UI.createDefault(peta, defaultLayers)

      // Add the click event listener
      addDraggableMarker(peta, behavior)
    })
  })
</script>
@endsection
