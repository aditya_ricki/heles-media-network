@extends('layouts.dashboard.app')

@section('title', 'Customers | HMN')

@section('main-content')
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Update Customer</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="{{ route('landing-page') }}"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="{{ route('marketing.customer') }}">Customers</a></li>
              <li class="breadcrumb-item active" aria-current="page">Update</li>
            </ol>
          </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
          <a href="{{ route('marketing.customer') }}" class="btn btn-sm btn-neutral"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid mt--6">
  <div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
      <div class="card">
        <div class="card-body">
          <form action="{{ route('marketing.customer.update', $customer->user->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input class="form-control" placeholder="Nama" type="text" name="name" value="{{ $customer->user->name }}" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                    </div>
                    <input class="form-control" placeholder="E-Mail" type="text" name="email" value="{{ $customer->user->email }}" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-phone"></i></span>
                    </div>
                    <input class="form-control" placeholder="No Telepon" type="text" name="phone" value="{{ $customer->phone }}">
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fab fa-whatsapp" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" placeholder="Whatsapp" type="text" name="whatsapp" value="{{ $customer->whatsapp }}" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-camera" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" type="file" name="photo" value="{{ $customer->photo }}">
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-id-card" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" type="file" name="photo_ktp" value="{{ $customer->photo_ktp }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=" fas fa-home" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" placeholder="Alamat" type="text" name="address" value="{{ $customer->address }}" required>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=" fas fa-map-marker-alt" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" placeholder="Longitude" type="text" name="longitude" value="{{ $customer->longitude }}" required id="longitude">
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group">
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class=" fas fa-map-marker-alt" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" placeholder="Latitude" type="text" name="latitude" value="{{ $customer->latitude }}" required id="latitude">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div style="width: 100%; height: 480px" id="map-container"></div>
              </div>
            </div>
            <hr>
            <div class="row mt-3">
              <div class="col-md-12 col-sm-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{ route('marketing.customer') }}" class="btn btn-secondary">Batal</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.dashboard.footer')
</div>
@endsection

@section('custom-js')
<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
<script>
  $(document).ready(function () {
    navigator.geolocation.getCurrentPosition(function(position) {
      let lastLong = document.getElementById('longitude').value
      let lastLat = document.getElementById('latitude').value

      let lat  = lastLat ? lastLat : position.coords.latitude
      let long = lastLong ? lastLong : position.coords.longitude

      function addDraggableMarker(peta, behavior){
        // create marker
        let marker = new H.map.Marker({
          lat: lat,
          lng: long
        },
        {
          // mark the object as volatile for the smooth dragging
          volatility: true
        })

        // Add the marker to the map and center the map at the location of the marker:
        peta.addObject(marker)

        // Ensure that the marker can receive drag events
        marker.draggable = true
        peta.addObject(marker)

        // disable the default draggability of the underlying map
        // and calculate the offset between mouse and target's position
        // when starting to drag a marker object:
        peta.addEventListener('dragstart', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer

          if (target instanceof H.map.Marker) {
            let targetPosition = peta.geoToScreen(target.getGeometry())
            target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y)
            behavior.disable()
          }
        }, false)

        // re-enable the default draggability of the underlying map
        // when dragging has completed
        peta.addEventListener('dragend', function(ev) {
          let target = ev.target
          document.getElementById('longitude').value = target.b.lng
          document.getElementById('latitude').value = target.b.lat

          if (target instanceof H.map.Marker) {
            behavior.enable()
          }
        }, false)

        // Listen to the drag event and move the position of the marker
        // as necessary
        peta.addEventListener('drag', function(ev) {
          let target = ev.target
          let pointer = ev.currentPointer
          if (target instanceof H.map.Marker) {
            target.setGeometry(peta.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y))
          }
        }, false)
      }

      let platform = new H.service.Platform({
        'apikey': '{{ env('HEREMAPS_API_KEY') }}'
      })

      // Obtain the default map types from the platform object:
      let defaultLayers = platform.createDefaultLayers()

      // Instantiate (and display) a map object:
      let peta = new H.Map(
        document.getElementById('map-container'),
        defaultLayers.vector.normal.map,
        {
          zoom: 15,
          center: {
            lat: lat,
            lng: long
          }
        }
      )

      // add a resize listener to make sure that the map occupies the whole container
      window.addEventListener('resize', () => peta.getViewPort().resize())

      // Add map events functionality to the map
      let mapEvents = new H.mapevents.MapEvents(peta)

      // Add behavior to the map: panning, zooming, dragging.
      let behavior = new H.mapevents.Behavior(mapEvents)

      // map ui
      let ui = H.ui.UI.createDefault(peta, defaultLayers)

      // Add the click event listener
      addDraggableMarker(peta, behavior)
    })
  })
</script>
@endsection

