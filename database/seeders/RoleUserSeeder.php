<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marketing = Role::create([
        	'code' => 'MRT',
        	'name' => 'Marketing',
        ]);

        $staffTeknis = Role::create([
        	'code' => 'STT',
        	'name' => 'Staff Teknis',
        ]);

        $finance = Role::create([
        	'code' => 'FNC',
        	'name' => 'Finance',
        ]);

        $customer = Role::create([
        	'code' => 'CST',
        	'name' => 'Customer',
        ]);

        User::create([
			'name'     => 'Mela Sintiya Dewi',
			'email'    => 'melasintiyadewi@gmail.com',
			'password' => Hash::make('12345678'),
			'role_id'  => $finance->id,
        ]);

        User::create([
			'name'     => 'Aditya Ricki',
			'email'    => 'adityaricki@gmail.com',
			'password' => Hash::make('12345678'),
			'role_id'  => $staffTeknis->id,
        ]);

        User::create([
			'name'     => 'Dede Komarudin',
			'email'    => 'dedekomarudin@gmail.com',
			'password' => Hash::make('12345678'),
			'role_id'  => $marketing->id,
        ]);

        User::create([
			'name'     => 'Yusuf Jaelani',
			'email'    => 'yusufjaelani@gmail.com',
			'password' => Hash::make('12345678'),
			'role_id'  => $customer->id,
        ]);
    }
}
